/*
	libsas7bdat: minimalist library to read sas7bdat files
	Copyright (C) 2012 Bertrand Marc

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef SAS7BDAT_SUBHEADER_H
#define SAS7BDAT_SUBHEADER_H

#include <stdint.h>

#define DATA_MODEL ILP32

#ifndef DATA_MODEL
#define SAS7BDAT_LONG long
#define SAS7BDAT_ULONG unsigned long
#else
#if DATA_MODEL == ILP32 /* 32 bit systems */
#define SAS7BDAT_LONG int32_t
#define SAS7BDAT_ULONG uint32_t
#elif DATA_MODEL == LLP64 /* 64 bit Windows */
#define SAS7BDAT_LONG int32_t
#define SAS7BDAT_ULONG uint32_t
#elif DATA_MODEL == LP64 /* 64 bit Unix and Unix-like systems (Linux, Mac OS X) */
#define SAS7BDAT_LONG int64_t
#define SAS7BDAT_ULONG uint64_t
#endif
#endif /* DATA_MODEL */

enum subheader_type_t {
	SUBH_ROWSIZE = (signed int) 0xF7F7F7F7,
	SUBH_COLSIZE = (signed int) 0xF6F6F6F6,
	SUBH_COLTEXT = (signed int) 0xFFFFFFFD,
	SUBH_COLATTR = (signed int) 0xFFFFFFFC,
	SUBH_COLNAME = (signed int) 0xFFFFFFFF,
	SUBH_COLLABS = (signed int) 0xFFFFFBFE,
	SUBH_COLLIST = (signed int) 0xFFFFFFFE,
	SUBH_SUBHCNT = (signed int) 0xFFFFFC00
};

struct row_size_subheader_t {
	enum subheader_type_t type;
	SAS7BDAT_LONG test[4];
	SAS7BDAT_LONG row_length;
	SAS7BDAT_LONG row_count;
	SAS7BDAT_LONG unknown_row_count[2];
	SAS7BDAT_LONG column_count1, column_count2;
	SAS7BDAT_LONG unknown2[2];
	SAS7BDAT_LONG page_size;
	SAS7BDAT_LONG unknown3;
	SAS7BDAT_LONG max_row_count;
	SAS7BDAT_ULONG end_of_header[2]; /* 0xFFFFFFFFFFFFFFFF */
	SAS7BDAT_LONG zeros[37];
	int page_signature[4];
	SAS7BDAT_LONG zeros2[7];
	SAS7BDAT_LONG unknown4;
	short unknown5;
	/* padding */
	SAS7BDAT_LONG pages_with_subheaders;
	short subheaders_with_positive_length_on_last_page;
	/* padding */
	SAS7BDAT_LONG pages_with_subheaders2;
	short subheaders_with_positive_length_on_last_page2;
	/* padding */
	SAS7BDAT_LONG page_count;
	short unknown6;
	/* padding */
	SAS7BDAT_LONG unknown7;
	short unknown8;
	/* padding */
	SAS7BDAT_LONG zeros3[10];
	short unknown9[9];
	short zeros4[4];
	short unknown10[5];
	int unknown11[9];
	short unknown12[2];
	short coltext_number, MXNAM, MXLAB;
	short zeros5[6];
	short data_rows_on_full_page;
	int todo2[7];
	int zeros6[3];
};

struct column_size_subheader_t {
	enum subheader_type_t type;
	int column_count, zero;
};

struct subheader_count_vector_t {
	SAS7BDAT_LONG signature, page1;
	short loc1;
	/* padding */
	SAS7BDAT_LONG pagel;
	short locl;
	/* padding */
};

struct subheader_count_subheader_t {
	enum subheader_type_t type;
	SAS7BDAT_LONG offset, unknown;
	short unknown2, zeros[3];
	SAS7BDAT_LONG unknown3[11];
	struct subheader_count_vector_t subheader_count_vector[12];
};

struct column_attributes_t {
	int offset, width;
	short name_length_flag;
	char type;
	char zero; /* padding ? */
};

struct column_attributes_subheader_t {
	enum subheader_type_t type;
	short remaining_length, zero[3];
	struct column_attributes_t column_attributes[];
};

struct column_format_label_subheader_t {
	enum subheader_type_t type;
	char unknown1[30];
	short format_index, format_offset, format_length, label_index, label_offset, label_length;
	char unknown2[6];
};

struct column_list_subheader_t {
	enum subheader_type_t type;
	short remaining_length1;
	char unknown1[6];
	short remaining_length2;
	char unknown2[2];
	short column_count, column_list_length, unknown3, column_count2;
	char unknown4[6];
	short column_list_values[];
};

struct column_name_pointer_t {
	short index, offset, length, sortedby;
};

struct column_name_subheader_t {
	enum subheader_type_t type;
	short remaining_length, zero[3];
	struct column_name_pointer_t column_name_pointer[];
};

struct column_text_subheader_t {
	enum subheader_type_t type;
	short remaining_length, zero;
	int unknown[2];
	char proc_name[60];
	char names[];
};

/* Subheaders used in mix and meta pages */
union subheader_t {
	enum subheader_type_t type;
	struct row_size_subheader_t row_size;
	struct column_size_subheader_t column_size;
	struct subheader_count_subheader_t subheader_count;
	struct column_text_subheader_t column_text;
	struct column_name_subheader_t column_name;
	struct column_attributes_subheader_t column_attributes;
	struct column_format_label_subheader_t column_format_label;
	struct column_list_subheader_t column_list;	
};

#endif /* SAS7BDAT_SUBHEADER_H */
