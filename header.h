/*
	libsas7bdat: minimalist library to read sas7bdat files
	Copyright (C) 2012 Bertrand Marc

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef SAS7BDAT_HEADER_H
#define SAS7BDAT_HEADER_H
// #define VERBOSE
struct header64_t {
	char magic[32];
	char align[4], word_size, endianness; /* There must the FLOAT_WORD_ORDER too */
	char OS_family[14]; /* Windows/Unix */
	char repeat[13]; /* zeros for release < 9, repeat of 28-40 for SAS v9 */
	char unknown3[5];
	short encoding;
	short unknown32;
	char repeat2[2]; /* zeros for release < 9, repeat of the last 2 bytes of OS_family */
	char zeros1[8];
	char SASFILE[8], dataset_name[64], file_type[8];
	char padding1[4];
	double timestamp[2];
	double unknown4_1[2]; /* time zone ? */
	int header_size, page_size, page_count;
	char zeros2[8];
	char release[8], host[64];
	int check[4]; /* 2=3=4=(1 XOR 0x656f4e4c) */
	int unknown6_2[4]; /* zeros ? not always*/
	int unknown6_3; /* first 4 bytes of a double (time) - ?????? XOR last 4 bytes of a double (time), not exactly the same as timestamp */
	/* The XOR mask seems to be correlated with release but not always, maybe arch ? or word size ?
		0x08caa000, often with version 7.0000M0
		0x6c000000, often with version 9.0202M0
		0xf0000000, often with version 9.0101M3 or 8.0202M0
		0xe8000000, often with 9.0202
		0x84000000, 9.0202M3X64_ESRV08
		plus exceptions for version 9.0000
	*/
	int zero3;
	char timestamp2[8]; /* or zeros for version 7 ? */
	char zeros4[684];
};

struct header_ILP32 {
	char magic[32];
	char align[4], word_size, endianness; /* There must the FLOAT_WORD_ORDER too */
	char OS_family[14]; /* Windows/Unix */
	char repeat[13]; /* zeros for release < 9, repeat of 28-40 for SAS v9 */
	char unknown3[5];
	short encoding;
	short unknown32;
	char repeat2[2]; /* zeros for release < 9, repeat of the last 2 bytes of OS_family */
	char zeros1[8];
	char SASFILE[8], dataset_name[64], file_type[8];
	double timestamp[2] __attribute__((packed));
	double unknown4_1[2] __attribute__((packed)); /* time zone ? */
	int header_size, page_size, page_count;
	char zeros2[8];
	char release[8], host[64];
	int check[4]; /* 2=3=4=(1 XOR 0x656f4e4c) */
	int unknown6_2[4]; /* zeros ? not always*/
	int unknown6_3; /* first 4 bytes of a double (time) - ?????? XOR last 4 bytes of a double (time), not exactly the same as timestamp */
	/* The XOR mask seems to be correlated with release but not always, maybe arch ? or word size ?
		0x08caa000, often with version 7.0000M0
		0x6c000000, often with version 9.0202M0
		0xf0000000, often with version 9.0101M3 or 8.0202M0
		0xe8000000, often with 9.0202
		0x84000000, 9.0202M3X64_ESRV08
		plus exceptions for version 9.0000
	*/
	int zero3;
	char timestamp2[8]; /* or zeros for version 7 ? */
	char zeros4[688];
};

#include "sas7bdat.h"

int read_header(SAS7BDAT*);
int _sas7bdat_init_header(SAS7BDAT*);

#endif /* SAS7BDAT_HEADER_H */
