/*
	libsas7bdat: minimalist library to read sas7bdat files
	Copyright (C) 2012 Bertrand Marc

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <stdint.h>

#include "foreign_system.h"
#if defined(_WIN32) || defined(_WIN64)
unsigned short bswap_16 (unsigned short __x) {
	unsigned short x;
	_swab ((const char*) &__x, (char*) &x, 16);
	return x;
}

unsigned int bswap_32 (unsigned int __x) {
	unsigned int x;
	_swab ((const char*) &__x, (char*) &x, 32);
	return x;
}

unsigned long long bswap_64 (unsigned long long __x) {
	unsigned long long x;
	_swab ((const char*) &__x, (char*) &x, 64);
	return x;
}
#endif /* _WIN32 || _WIN64 */

/* Convert a 32bits header to a 64bits header */
void convert_32(struct header64_t *header64) {
#ifdef VERBOSE
	printf("Conversion from 32bits\n");
#endif
	memmove(&(header64->timestamp), (void*) &(header64->timestamp) - 4, sizeof(struct header64_t) - offsetof(struct header64_t, timestamp) );
}

static double bswap_double(double d) {
	unsigned long long *i = (unsigned long long*) &d;
	*i = bswap_64(*i);
	return d;
}

/* Enough to read the (8096 bytes) Sun OS header */
void convert_SunOS(struct header64_t *header) {
#ifdef VERBOSE
	printf("Conversion from Sun OS\n");
#endif
	memmove(&(header->page_count), (char*) &(header->page_count) + 4, sizeof(struct header64_t) - offsetof(struct header64_t, page_count) - 4);
}

void bswap_header(struct header64_t *header) {
	header->page_size = bswap_32(header->page_size);
	header->page_count = bswap_32(header->page_count);
	header->timestamp[0] = bswap_double(header->timestamp[0]);
	header->timestamp[1] = bswap_double(header->timestamp[1]);
		
	header->header_size = bswap_32(header->header_size);
	header->check[0] = bswap_32(header->check[0]);
	header->check[1] = bswap_32(header->check[1]);
	header->check[2] = bswap_32(header->check[2]);
	header->check[3] = bswap_32(header->check[3]);

	*((double*) header->timestamp2) = bswap_double(*((double*) header->timestamp2));
	
	header->unknown6_3 = bswap_32(header->unknown6_3);
}

/* Change endianness in pages */
void bswap_page(union page_t *page) {
	page->any.unknown1[0] = bswap_32(page->any.unknown1[0] );
	page->any.type = bswap_16(page->any.type);
	int i;
	switch (page->any.type) {
		case(META):
		case(MIX):
			page->meta.nb_subheader_pointers = bswap_16(page->meta.nb_subheader_pointers);
			page->meta.nb_data_block = bswap_16(page->meta.nb_data_block);
			for (i = 0; i < page->meta.nb_subheader_pointers; ++i) {
				page->meta.subheader_pointers[i].offset = bswap_32(page->meta.subheader_pointers[i].offset);
				page->meta.subheader_pointers[i].length = bswap_32(page->meta.subheader_pointers[i].length);
				page->meta.subheader_pointers[i].unknown = bswap_32(page->meta.subheader_pointers[i].unknown);
			}
			break;
		case(DATA):
			break;
		case(AMD):
			break;
	}
}

/* Change endianness in subheaders */
void bswap_subheader(union subheader_t *subheader) {
	subheader->type = bswap_32(subheader->type);
	int i, CMAX;
	switch (subheader->type) {
		case(SUBH_ROWSIZE):
			subheader->row_size.row_length = bswap_32(subheader->row_size.row_length);
			subheader->row_size.row_count = bswap_32(subheader->row_size.row_count);
			subheader->row_size.page_size = bswap_32(subheader->row_size.page_size);
			subheader->row_size.test[0] = bswap_32(subheader->row_size.test[0]);
			subheader->row_size.test[1] = bswap_32(subheader->row_size.test[1]);
			subheader->row_size.test[2] = bswap_32(subheader->row_size.test[2]);
			subheader->row_size.test[3] = bswap_32(subheader->row_size.test[3]);
			subheader->row_size.column_count1 = bswap_32(subheader->row_size.column_count1);
			subheader->row_size.column_count2 = bswap_32(subheader->row_size.column_count2);
			break;
		case(SUBH_COLSIZE):
			subheader->column_size.column_count = bswap_32(subheader->column_size.column_count);
			break;
		case(SUBH_COLTEXT):
			subheader->column_text.remaining_length = bswap_16(subheader->column_text.remaining_length);
			break;
		case(SUBH_COLATTR):
			subheader->column_attributes.remaining_length = bswap_16(subheader->column_attributes.remaining_length);
			CMAX = (subheader->column_attributes.remaining_length - 8)/sizeof (struct column_attributes_t);
			for (i = 0; i < CMAX; ++i) {
				struct column_attributes_t* attributes = subheader->column_attributes.column_attributes + i;
				attributes->offset = bswap_32(attributes->offset);
				attributes->width = bswap_32(attributes->width);
				attributes->name_length_flag = bswap_16(attributes->name_length_flag);
			}
			break;
		case(SUBH_COLNAME):
			subheader->column_name.remaining_length = bswap_16(subheader->column_name.remaining_length);
			CMAX = (subheader->column_name.remaining_length - 8)/sizeof (struct column_name_pointer_t);
			for (i = 0; i < CMAX; ++i) {
				struct column_name_pointer_t* cnp = subheader->column_name.column_name_pointer + i;
				cnp->index = bswap_16(cnp->index);
				cnp->offset = bswap_16(cnp->offset);
				cnp->length = bswap_16(cnp->length);
				cnp->sortedby = bswap_16(cnp->sortedby);
			}
			break;
		case(SUBH_COLLABS):
			break;
		case(SUBH_COLLIST):
			break;
		case(SUBH_SUBHCNT):
			for (i = 0; i < 11; ++i) {
				struct subheader_count_vector_t *cv = subheader->subheader_count.subheader_count_vector + i;
			}
			break;
	}
}
