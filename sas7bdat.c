/*
	libsas7bdat: minimalist library to read sas7bdat files
	Copyright (C) 2012 Bertrand Marc

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include "header.h"
#include "subheader.h"
#include "page.h"
#include "data.h"
#include "sas7bdat_struct.h"
#include "sas7bdat.h"

/* Read meta information, copy it to the dataset and point the first page with data */
static int read_meta(struct sas7bdat_handler_t *dataset) {
	dataset->column_attributes.length = 0;
	int res;
	for (res = read_next_page(dataset); res == EXIT_SUCCESS && dataset->reader.page->any.type == META; res = read_next_page(dataset)) {
		res = extract_meta(dataset);
		if (res != EXIT_SUCCESS)
			return res;
	}
	if (res != EXIT_SUCCESS)
		return res;
	/* At this point, the cached page is the first page with binary data (MIX or DATA) */
	if (dataset->reader.page->any.type == MIX) {
		res = extract_meta(dataset);
		if (res != EXIT_SUCCESS)
			return res;
	}
	if (dataset->row_size.type != SUBH_ROWSIZE || dataset->col_size.type != SUBH_COLSIZE) {
		fprintf(stderr, "Error: could not find ROWSIZE or COLSIZE subheader.\n");
		return EXIT_FAILURE;
	}
	if (dataset->reader.page->any.type == MIX) {
		if (dataset->row_size.row_count < dataset->row_size.max_row_count)
			dataset->reader.remaining_rows = dataset->row_size.row_count;
		else
			dataset->reader.remaining_rows = dataset->row_size.max_row_count;
	}
	return EXIT_SUCCESS;
}

/* Create a SAS7BDAT handler */
SAS7BDAT* sas7bdat_init() {
	SAS7BDAT *dataset = (SAS7BDAT*) calloc(1, sizeof(SAS7BDAT)); /* Make sure it is initialised to 0 */
	dataset->fp = NULL;
	return dataset;
}

/* Open a dataset */
int sas7bdat_open(SAS7BDAT *dataset, const char *filename) {
	dataset->filename = filename;
	dataset->fp = fopen(filename, "rb");
	if(dataset->fp == NULL) {
		fprintf(stderr, "Error: could not open file %s, %s.\n", filename, strerror(errno));
		return EXIT_FAILURE;
	}

	size_t read = fread(&dataset->header, sizeof(struct header64_t), 1, dataset->fp);
	if (read != 1) {
		if ( feof(dataset->fp) )
			fprintf(stderr, "Error: unexpected end of file %s.\n", filename);
		if ( ferror(dataset->fp) )
			fprintf(stderr, "Error: reading file %s failed.\n", filename);
		sas7bdat_close(dataset);
		return EXIT_FAILURE;
	}

	/* Check the header */
	if (read_header(dataset) != EXIT_SUCCESS) {
		sas7bdat_close(dataset);
		return EXIT_FAILURE;
	}

	/* Handle headers longer than 1024 */
	if (dataset->header.header_size != sizeof(struct header64_t))
		fseek(dataset->fp, dataset->header.header_size, SEEK_SET);

	dataset->reader.page = malloc(sizeof(char) * dataset->header.page_size);
	dataset->meta_pages = malloc(dataset->header.page_size);
	dataset->column_text_subheaders.offset = malloc(sizeof(int));
	dataset->column_name_subheaders.offset = malloc(sizeof(int));
	if ( dataset->reader.page == NULL || dataset->meta_pages == NULL
		|| dataset->column_text_subheaders.offset == NULL
		|| dataset->column_name_subheaders.offset == NULL) {
		fprintf(stderr, "Error: memory could not be allocated.\n");
		sas7bdat_close(dataset);
		return EXIT_FAILURE;
	}

	/* Read and extract META pages */
	if (read_meta(dataset) != EXIT_SUCCESS) {
		sas7bdat_close(dataset);
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

/* Close a dataset */
int sas7bdat_close(SAS7BDAT *dataset) {
	free(dataset->reader.page);
	free(dataset->meta_pages);
	free(dataset->column_attributes.attributes);
	free(dataset->column_text_subheaders.offset);
	free(dataset->column_name_subheaders.offset);
	int res;
	if(dataset->fp != NULL)
		res = fclose(dataset->fp);
	else
		res = EXIT_SUCCESS;
	free(dataset);
	return res;
}

/* Get the number of variables */
int sas7bdat_variables_number(SAS7BDAT *dataset) {
	return dataset->col_size.column_count; /* May be different than CC1+CC2 in the ROWSIZE subheader */
}

/* Get the variables names and apply callback */
int sas7bdat_variables_names(SAS7BDAT *dataset, int (*callback_char)(void*, int, int, const char*), void *ptr) {
	if (dataset->column_text_subheaders.length == 0 || dataset->column_name_subheaders.length == 0) {
		fprintf(stderr, "Error: no column name.\n");
		return EXIT_FAILURE;
	}
	int i, j, CMAX, p = 0;
	for (i = 0; i < dataset->column_name_subheaders.length; ++i) {
		struct column_name_subheader_t *cns = (struct column_name_subheader_t *) (dataset->meta_pages->raw + dataset->column_name_subheaders.offset[i]);
		CMAX = (cns->remaining_length - 8)/sizeof (struct column_name_pointer_t);
		for (j = 0; j < CMAX; ++j) {
			struct column_name_pointer_t* cnp = cns->column_name_pointer + j;
			struct column_text_subheader_t *cts = (struct column_text_subheader_t *) (dataset->meta_pages->raw + dataset->column_text_subheaders.offset[cnp->index]);
			callback_char(ptr, p + 1, cnp->length,  ((char*) cts) + cnp->offset + 4);
			++p;
		}
	}
	return EXIT_SUCCESS;
}

/* Get the number of rows */
int sas7bdat_row_count(SAS7BDAT *dataset) {
	return dataset->row_size.row_count;
}

/* Read the next row, and apply callbacks (one for double, one for char*) */
int sas7bdat_next_row(SAS7BDAT *dataset, int (*callback_num)(void*, int, double), int (*callback_char)(void*, int, int, const char*), void *ptr) {
	if (dataset->reader.remaining_rows == 0) {
		if (dataset->reader.page_n == dataset->header.page_count)
			return EXIT_FAILURE; /* last page */
		int res = read_next_page(dataset);
		if (res != EXIT_SUCCESS)
			return res;
	}
	int i;
	for (i = 0; i < dataset->column_attributes.length; ++i) {
		if (dataset->column_attributes.attributes[i].type == 1) {
			/* TODO test callbacks result */
			callback_num(ptr, i + 1, read_SAS_numeric(dataset->reader.row, dataset->column_attributes.attributes[i].width));
		} else if (dataset->column_attributes.attributes[i].type == 2)
			callback_char(ptr, i + 1, dataset->column_attributes.attributes[i].width, dataset->reader.row);
		else {
			fprintf(stderr, "Error: unknown column type.\n");
			return EXIT_FAILURE;
		}
		dataset->reader.row += dataset->column_attributes.attributes[i].width;
	}
	--dataset->reader.remaining_rows;
	++dataset->reader._n_;
	return EXIT_SUCCESS;
}

/* SAS time is a number of seconds since Jan 1, 1960 */
static struct tm SAS_origin = { .tm_sec = 0, .tm_min = 0, .tm_hour = 0, .tm_mday = 1, .tm_mon = 0, .tm_year = 60, .tm_isdst = -1};

/* Convert a SAS time to a standard time_t */
time_t read_SAS_time(double SAS_time) {
	time_t origin = mktime(&SAS_origin);
	assert(origin != (time_t) -1);
	return SAS_time + origin;
}

/* Convert a standard time_t to a SAS time */
double write_SAS_time(time_t unix_time) {
	time_t origin = mktime(&SAS_origin);
	assert(origin != (time_t) -1);
	return difftime(unix_time, origin);
}

