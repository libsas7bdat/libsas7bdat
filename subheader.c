/*
	libsas7bdat: minimalist library to read sas7bdat files
	Copyright (C) 2012 Bertrand Marc

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include <stdlib.h>

#include "subheader.h"
#define PAGE_SIZE 4096

struct row_size_subheader_t _sas7bdat_init_row_size_subheader(int row_length, int row_count, int column_count, int page_size) {
	struct row_size_subheader_t sh = {
		.type = SUBH_ROWSIZE,
		.row_length = row_length,
		.row_count = row_count,
		.column_count1 = column_count,
		.column_count2 = 0,
		.page_size = page_size,
		.max_row_count = page_size/row_count,
		.end_of_header = { -1, -1 }
	};
	return sh;
}

struct column_size_subheader_t _sas7bdat_init_column_size_subheader(int column_count) {
	struct column_size_subheader_t subh = { .type = SUBH_COLSIZE, .column_count = column_count };
	return subh;
}

struct column_attributes_subheader_t* _sas7bdat_init_column_attributes_subheader(int column_count) {
	struct column_attributes_subheader_t *subh = malloc(sizeof(struct column_attributes_subheader_t) + column_count * sizeof(struct column_attributes_t));
	subh->type = SUBH_COLATTR;
	/* subh->column_attributes[0].offset subh->column_attributes[0].width */
	return subh;
}

struct column_name_subheader_t* _sas7bdat_init_column_name_subheader(int column_count) {
	struct column_name_subheader_t *subh = malloc(sizeof(struct column_attributes_subheader_t) + column_count * sizeof(struct column_attributes_t));
	subh->type = SUBH_COLNAME;
	/* subh->column_name_pointer[0].offset subh->column_name_pointer[0].width */
	return subh;
}
