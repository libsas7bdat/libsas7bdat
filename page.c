/*
	libsas7bdat: minimalist library to read sas7bdat files
	Copyright (C) 2012 Bertrand Marc

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdlib.h>
#include <string.h>

#include "sas7bdat_struct.h"
#include "subheader.h"
#include "page.h"

int extract_meta(struct sas7bdat_handler_t *dataset) {
	/* Copy page from cache to meta_pages */
	++dataset->n_meta_pages;
	if (dataset->n_meta_pages > 1) {
		void *ptr = realloc(dataset->meta_pages, (size_t) dataset->header.page_size * dataset->n_meta_pages);
		if (ptr == NULL) {
			fprintf(stderr, "Error: could not realloc memory.\n");
			return EXIT_FAILURE;
		}
		dataset->meta_pages = ptr;
	}
	union page_t *page = (union page_t*) (dataset->meta_pages->raw + (dataset->n_meta_pages - 1) * dataset->header.page_size);
	memcpy(page, dataset->reader.page, dataset->header.page_size);
	int i;
	for (i = 0; i < page->meta.nb_subheader_pointers; ++i) {
		if (page->meta.subheader_pointers[i].length == 0)
			continue; /* ignore empty subheader */
		if ( page->meta.subheader_pointers[i].offset > dataset->header.page_size ) {
			fprintf(stderr, "Error: subheader outside of range.\n");
			return EXIT_FAILURE;
		}
		union subheader_t *subheader = (union subheader_t*) (page->raw + page->meta.subheader_pointers[i].offset);
		size_t length;
		int old_attributes_nb;
		switch (subheader->type) {
			case(SUBH_ROWSIZE):
				dataset->row_size = subheader->row_size;
				break;
			case(SUBH_COLSIZE):
				dataset->col_size = subheader->column_size;
				break;
			case(SUBH_COLATTR):
				length = subheader->column_attributes.remaining_length - 8;
				old_attributes_nb = dataset->column_attributes.length;
				if (dataset->column_attributes.length == 0) {
					dataset->column_attributes.length = length/sizeof (struct column_attributes_t);
					dataset->column_attributes.attributes = malloc(sizeof(struct column_attributes_t) * dataset->column_attributes.length);
					memcpy(dataset->column_attributes.attributes, subheader->column_attributes.column_attributes, length);
				} else {
					dataset->column_attributes.length += length/sizeof (struct column_attributes_t);
					void *ptr = realloc(dataset->column_attributes.attributes, sizeof(struct column_attributes_t) * dataset->column_attributes.length);
					if (ptr == NULL) {
						fprintf(stderr, "Error: could not realloc memory.\n");
						return EXIT_FAILURE;
					}
					dataset->column_attributes.attributes = ptr;
					memcpy(dataset->column_attributes.attributes + old_attributes_nb, subheader->column_attributes.column_attributes, length);
				}
				break;
			case(SUBH_COLTEXT):
				++dataset->column_text_subheaders.length;
				if (dataset->column_text_subheaders.length > 1) {
					void *ptr = realloc(dataset->column_text_subheaders.offset, sizeof(int) * dataset->column_text_subheaders.length);
					if (ptr == NULL) {
						fprintf(stderr, "Error: could not realloc memory.\n");
						return EXIT_FAILURE;
					}
					dataset->column_text_subheaders.offset = ptr;
				}
				dataset->column_text_subheaders.offset[dataset->column_text_subheaders.length - 1] = (char*) subheader - (char*) dataset->meta_pages;
				break;
			case(SUBH_COLNAME):
				++dataset->column_name_subheaders.length;
				if (dataset->column_name_subheaders.length > 1) {
					void *ptr = realloc(dataset->column_name_subheaders.offset, sizeof(int) * dataset->column_name_subheaders.length);
					if (ptr == NULL) {
						fprintf(stderr, "Error: could not realloc memory.\n");
						return EXIT_FAILURE;
					}
					dataset->column_name_subheaders.offset = ptr;
				}
				dataset->column_name_subheaders.offset[dataset->column_name_subheaders.length - 1] = (char*) subheader - (char*) dataset->meta_pages;
				break;
			default:
				break;
		}
	}
	return EXIT_SUCCESS;
}

int read_next_page(struct sas7bdat_handler_t *dataset) {
	size_t read = fread(dataset->reader.page, dataset->header.page_size, 1, dataset->fp);
	if (read != 1) {
		if ( feof(dataset->fp) )
			fprintf(stderr, "File too short %s.\n", dataset->filename);
		if ( ferror(dataset->fp) )
			fprintf(stderr, "Error reading file %s.\n", dataset->filename);
		return EXIT_FAILURE;
	}
	++dataset->reader.page_n;
	if (dataset->reader.page->any.type == MIX) {
		size_t m = 24 + dataset->reader.page->mix.nb_subheader_pointers * 12;
		m += m % 8;
		dataset->reader.row = dataset->reader.page->raw + m;
	} else if (dataset->reader.page->any.type == DATA) {
		dataset->reader.row = dataset->reader.page->data.binary;
		dataset->reader.remaining_rows = dataset->reader.page->data.row_count;
	}
	return EXIT_SUCCESS;
}
