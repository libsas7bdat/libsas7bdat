CC=gcc
CFLAGS=-Wall -Wextra -pedantic -std=c99
LDFLAGS=
SRC= $(wildcard *.c)
OBJ= $(SRC:.c=.o)
EXEC=libsas7bdat

all: $(EXEC)

write: $(OBJ) examples/write.o
	$(CC) -o $@ $^ $(LDFLAGS)

$(EXEC): $(OBJ) examples/test.o
	$(CC) -o $@ $^ $(LDFLAGS)

csv: $(OBJ) examples/sas7bdat2csv.o
	$(CC) -o $(EXEC) $^ $(LDFLAGS)

static: $(OBJ)
	ar rcs $(EXEC).a $(OBJ)

%.o: %.c %d
	$(CC) -o $@ -c $< $(CFLAGS)

clean:
	rm -f *.o *.a $(EXEC) *.exe *.d *.stackdump test.log test.csv examples/*.o examples/*.d

test: $(EXEC)
	@for file in ../datasets/*.sas7bdat ; do \
		# echo $$file ; \
		./libsas7bdat $$file ; \
	done > test.log 2>&1

run: $(EXEC)
	./$(EXEC)

memtest: csv
	valgrind --leak-check=yes ./$(EXEC) ../datasets/applican.sas7bdat test.csv

.PHONY: clean run tarball

-include $(SRC:.c=.d)

%.d: %.c
	@$(CC) -MM -MF $@ $^ $(CFLAGS)

tarball: clean
	tar -cjf ../$(EXEC).tar.bz2 .
