/*
	libsas7bdat: minimalist library to read sas7bdat files
	Copyright (C) 2012 Bertrand Marc

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef SAS7BDAT_STRUCT_H
#define SAS7BDAT_STRUCT_H

#include <stdio.h>

#include "header.h"
#include "subheader.h"

struct subheader_array_t {
	short length;
	int *offset;
};

struct column_attributes_array_t {
	int length;
	struct column_attributes_t *attributes;
};

struct sas7bdat_reader_t {
	union page_t *page;
	int page_n;
	int _n_;
	int remaining_rows;
	char *row;
};

struct sas7bdat_handler_t {
	const char *filename;
	FILE *fp;
	struct header_ILP32 header;
	union page_t *meta_pages;
	int n_meta_pages;
	struct row_size_subheader_t row_size;
	struct column_size_subheader_t col_size;
	struct column_attributes_array_t column_attributes;
	struct sas7bdat_reader_t reader;
	struct subheader_array_t column_text_subheaders;
	struct subheader_array_t column_name_subheaders;
};

#endif
