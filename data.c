/*
	libsas7bdat: minimalist library to read sas7bdat files
	Copyright (C) 2012 Bertrand Marc

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdio.h>
#include <string.h>
#include "data.h"

double read_SAS_numeric(const void *ptr, size_t size) {
	double tmp = 0;
	memcpy((char*) &tmp + sizeof(double) - size, ptr, size);
	return tmp;
}
