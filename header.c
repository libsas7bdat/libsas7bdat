/*
	libsas7bdat: minimalist library to read sas7bdat files
	Copyright (C) 2012 Bertrand Marc

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>

#include "foreign_system.h"
#include "header.h"
#include "sas7bdat_struct.h"
#include "sas7bdat.h"

#define HEADER_SIZE 1024

/* Read header and extract infos */
int read_header(struct sas7bdat_handler_t *dataset) {
	/* Check Magic number */
	const unsigned char magic[32] = {
		'\x0' , '\x0' , '\x0' , '\x0' , '\x0' , '\x0' , '\x0' , '\x0' , 
		'\x0' , '\x0' , '\x0' , '\x0' , '\xc2', '\xea', '\x81', '\x60',
		'\xb3', '\x14', '\x11', '\xcf', '\xbd', '\x92', '\x8' , '\x0' ,
		'\x9' , '\xc7', '\x31', '\x8c', '\x18', '\x1f', '\x10', '\x11'
	};
	if ( memcmp(magic, dataset->header.magic, 32) ) {
		fprintf(stderr, "Not a valid SAS file (magic number mismatch).\n");
		return EXIT_FAILURE;
	}
	
	/* bytes 33-38 */
	assert(dataset->header.align[0] == '\x22' || dataset->header.align[0] == '\x33'); /* Alignement of page_count: 0/+4 */
	assert(dataset->header.align[1] == '\x22');
	assert(dataset->header.align[2] == '\x00');
	assert(dataset->header.align[3] == '\x32' || dataset->header.align[3] == '\x33'); /* Alignement of timestamp: 0/+4 */
	assert(dataset->header.word_size == '\x22' || dataset->header.word_size == '\x33'); /* Word size : 32/64bits */
	assert(dataset->header.endianness == '\x00' || dataset->header.endianness == '\x01'); /* Endianness : big/little */
	
	/*if (dataset->header.align[3] == '\x32')
		convert_32(&dataset->header); */
	if (dataset->header.align[0] == '\x33')
		convert_SunOS(&dataset->header);

	if (dataset->header.endianness == 0) /* big endian */
		bswap_header(&dataset->header);

	/* Check validity */
	if ( memcmp("SAS FILE", dataset->header.SASFILE, 8) ) {
		fprintf(stderr, "Not a valid SAS file (%.8s).\n", dataset->header.SASFILE);
		return EXIT_FAILURE;
	}
	if ( memcmp("DATA    ", dataset->header.file_type, 8) ) {
		fprintf(stderr, "Not a SAS DATA file (%.8s).\n", dataset->header.file_type);
		return EXIT_FAILURE;
	}

#ifdef VERBOSE
	if (dataset->header.unknown6_2[1] != 0)
		printf("Unknown byte 316\n");
	time_t creation_time = read_SAS_time(dataset->header.timestamp[0]);
	time_t change_time = read_SAS_time(dataset->header.timestamp[1]);
	printf("Creation time: %s", ctime(&creation_time));
	printf("Change time: %s", ctime(&change_time));

	printf("%.64s\n", dataset->header.dataset_name);
	printf("Header size = %d, PS = %d, PC = %d\n", dataset->header.header_size, dataset->header.page_size, dataset->header.page_count);
	printf("release: %.8s host: %.64s\n", dataset->header.release, dataset->header.host);
#endif
	
	/* bytes 38-51 */
	assert( strncmp(dataset->header.OS_family, "\0022\004\0\0\0\0\0\0\0\0\0\003\001", 14) == 0 /* Win */
		|| strncmp(dataset->header.OS_family, "\0021\001\0\0\0\0\0\0\0\0\0\002\001", 14) == 0 ); /* Unix */

	/* bytes 52-64 */
	assert( (atof(dataset->header.release) >= 9)
		|| (memcmp(dataset->header.repeat, "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 24) == 0) );

	/* bytes 52-64 */
	assert( (atof(dataset->header.release) < 9)
		|| (memcmp(dataset->header.align - 4, dataset->header.repeat, 13) == 0) );
	
	/* bytes 65-69 */
	assert( (atof(dataset->header.release) < 9)
		|| (strncmp(dataset->header.unknown3, "2\001\"\"\0", 5) == 0)
		|| (strncmp(dataset->header.unknown3, "3\001#\"\0", 5) == 0) /* eyecarex only (Stat/transfert) */
		|| (strncmp(dataset->header.unknown3, "3\001#3\0", 5) == 0)); /* 64bits + Sun OS ? */

	/* bytes 71-76 */
	assert( (atof(dataset->header.release) < 9)
		|| (dataset->header.encoding == 20) /* utf-8 */
		|| (dataset->header.encoding == 28) /* ascii */
		|| (dataset->header.encoding == 29) /* latin1 */
		|| (dataset->header.encoding == 30) /* latin3 */
		|| (dataset->header.encoding == 31) /* latin3 */
		|| (dataset->header.encoding == 40) /* latin9 */
		|| (dataset->header.encoding == 62) /* wlatin1 */
		|| (dataset->header.encoding == 60) /* wlatin2 */
		|| (dataset->header.encoding == 0)); /* Default */
	assert( (atof(dataset->header.release) < 9)
		|| (dataset->header.unknown32 == 0x1000) /* Windows ? */
		|| (strncmp(dataset->header.dataset_name, "EYECAREX", 8) == 0 && dataset->header.unknown32 == 0)
		|| (dataset->header.unknown32 == 0x2000) /* Unix ? */
		|| (dataset->header.unknown32 == 0x2100)); /* Unix ? */
	assert( (atof(dataset->header.release) < 9)
		|| (memcmp(dataset->header.repeat2, dataset->header.repeat - 2, 2) == 0) );

	/* bytes 77-84 */
	assert( strncmp(dataset->header.zeros1, "\0\0\0\0\0\0\0\0", 8) == 0 );

	assert((int) dataset->header.unknown4_1[0] % 3600 == 0 && abs(dataset->header.unknown4_1[0]) <= 12 * 3600 ); /* divisible par 3600, < 12h */

	assert( dataset->header.unknown4_1[0] == dataset->header.unknown4_1[1]);
	assert( dataset->header.header_size == 1024 || dataset->header.header_size == 8192 );
	assert( strncmp(dataset->header.zeros2, "\0\0\0\0\0\0\0\0", 8) == 0
		|| strncmp(dataset->header.zeros2, "\001\0\0\0\0\0\0\0", 8) == 0);
	
	/* bytes 292-307 */
	assert( ((atof(dataset->header.release) < 8) && (dataset->header.check[0] == 0x49f04e93 ))
		|| ((atof(dataset->header.release) >= 8) && (*((int*) dataset->header.timestamp) == dataset->header.check[0])) 
		|| ((strncmp(dataset->header.dataset_name, "EYECAREX", 8) == 0) && (*(((int*) dataset->header.timestamp) + 1) == dataset->header.check[0])) );
	assert( dataset->header.check[1] == dataset->header.check[2] );
	assert( dataset->header.check[1] == dataset->header.check[3] );
	
	int mask = 0x656f4e4c;
	if (dataset->header.endianness == '\x00') /* Big endian */
		mask = bswap_32(mask);
	assert( (dataset->header.check[0] ^ mask) == dataset->header.check[1] );
	
	/* bytes 308-323 */
	// printf("%08x, %08x, %08x, %08x\n", dataset->header.unknown6_2[0], dataset->header.unknown6_2[1], dataset->header.unknown6_2[2], dataset->header.unknown6_2[3]);
	assert( dataset->header.unknown6_2[0] == 0);
	assert( dataset->header.unknown6_2[1] == 0 || dataset->header.unknown6_2[1] == 1); /* Time zone ? */
	assert( dataset->header.unknown6_2[2] == 0);
	assert( dataset->header.unknown6_2[3] == 0);
	
	assert( (atof(dataset->header.release) >= 8) || dataset->header.unknown6_3 == 0);

// #ifdef VERBOSE
	/* bytes 324-327 */
	printf("%08x, %08x, %08x, %08x, %08x",
		dataset->header.check[0], *((int*) dataset->header.timestamp + 1),
		dataset->header.check[0] - (dataset->header.unknown6_3^ *((int*) dataset->header.timestamp + 1)),
		dataset->header.unknown6_3, (dataset->header.check[0]- 0xf0000000) /*dataset->header.unknown6_3 */^ *((int*) dataset->header.timestamp + 1));
	/* Seems to be 0x08000000, 0x6c000000, 0xf0000000, 0xe8000000...*/
	printf(",%s, %x, %x\n", dataset->header.release, dataset->header.word_size, dataset->header.OS_family[1]);
// #endif

	/* bytes 328-331 */
	assert( dataset->header.zero3 == 0 );
	/* bytes 332-339 */
	assert( (atof(dataset->header.release) < 9)
		|| memcmp(dataset->header.timestamp, dataset->header.timestamp2, 8) == 0
		|| strncmp(dataset->header.timestamp2, "\0\0\0\0\0\0\0\0", 8) == 0 );
	
	if (dataset->header.align[0] == '\x33' && dataset->header.endianness == 0)
		return EXIT_FAILURE; /* Unix 64 bits => sizeof(int) = 8 in pages : offsetof(type) = 32, maybe sizeof(subheader_pointer) = 24*/
	
	return EXIT_SUCCESS;
}

int _sas7bdat_init_header(struct sas7bdat_handler_t *dataset) {
	const unsigned char magic[32] = {
		'\x0' , '\x0' , '\x0' , '\x0' , '\x0' , '\x0' , '\x0' , '\x0' , 
		'\x0' , '\x0' , '\x0' , '\x0' , '\xc2', '\xea', '\x81', '\x60',
		'\xb3', '\x14', '\x11', '\xcf', '\xbd', '\x92', '\x8' , '\x0' ,
		'\x9' , '\xc7', '\x31', '\x8c', '\x18', '\x1f', '\x10', '\x11'
	};
	memcpy(&dataset->header.magic, magic, 32);

	dataset->header.align[0] = '\x22';
	dataset->header.align[1] = '\x22';
	dataset->header.align[2] = '\x00';
	dataset->header.align[3] = '\x32'; /* Alignement of timestamp: 0/+4 */
	dataset->header.word_size = '\x22'; /* Word size : 32bits */
#ifdef WORDS_BIGENDIAN /* Endianness */
	dataset->header.endianness = '\x00';
#else
	dataset->header.endianness = '\x01';
#endif

//#if defined _WIN64 || defined _WIN32
	memcpy(&dataset->header.OS_family, "\0022\004\0\0\0\0\0\0\0\0\0\003\001", 14);
/*#else
	memcpy(&dataset->header.OS_family, "\0021\001\0\0\0\0\0\0\0\0\0\002\001", 14);
#endif*/
	memcpy(&dataset->header.repeat, dataset->header.align - 4, 13);
	memcpy(&dataset->header.unknown3, "2\001\"\"\0", 5);

	dataset->header.encoding = 62; /* ascii */
	dataset->header.unknown32 = 0x1000;
	memcpy(&dataset->header.repeat2, dataset->header.repeat - 2, 2);

	memcpy(&dataset->header.SASFILE, "SAS FILE", 8);
	strncpy(dataset->header.dataset_name, "MACHIN                                                          ", sizeof(dataset->header.dataset_name));
	memcpy(&dataset->header.file_type, "DATA    ", 8);

	time_t now = time(NULL);

	dataset->header.timestamp[0] = write_SAS_time(now);
	dataset->header.timestamp[1] = dataset->header.timestamp[0];

	dataset->header.unknown4_1[0] = difftime(now, mktime(gmtime(&now)));
	dataset->header.unknown4_1[1] = dataset->header.unknown4_1[0];

	dataset->header.header_size = HEADER_SIZE;
	dataset->header.page_size = 4096;
	dataset->header.page_count = 1;

	strncpy(dataset->header.release, "9.0101M3", sizeof(dataset->header.release));
	strncpy(dataset->header.host, "XP_PRO", sizeof(dataset->header.host));

	dataset->header.check[0] = *((int*) dataset->header.timestamp);
	dataset->header.check[1] = dataset->header.check[0] ^ 0x656f4e4c;
	dataset->header.check[2] = dataset->header.check[1];
	dataset->header.check[3] = dataset->header.check[1];

	dataset->header.unknown6_3 = 1166162672;
	memcpy(dataset->header.timestamp2, dataset->header.timestamp, 8);
	return EXIT_SUCCESS;
}

