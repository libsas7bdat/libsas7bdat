/*
	libsas7bdat: minimalist API to read sas7bdat files
	Copyright (C) 2012 Bertrand Marc

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef SAS7BDAT_H
#define SAS7BDAT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <time.h>

typedef struct sas7bdat_handler_t SAS7BDAT;

/* Create a SAS7BDAT handler */
SAS7BDAT* sas7bdat_init();

/* Open a dataset */
int sas7bdat_open(SAS7BDAT*, const char*);

/* Close a dataset */
int sas7bdat_close(SAS7BDAT*);

/* Get the number of variables */
int sas7bdat_variables_number(SAS7BDAT*);

/* Get the variables names and apply callback */
int sas7bdat_variables_names(SAS7BDAT*, int (*)(void*, int, int, const char*), void*);

/* Get the number of rows */
int sas7bdat_row_count(SAS7BDAT*);

/* Read the next raw, and apply callbacks (one for double, one for char*) */
int sas7bdat_next_row(SAS7BDAT*, int (*)(void*, int, double), int (*)(void*, int, int, const char*), void*);

/* convert SAS Datetime to time_t */
time_t read_SAS_time(double);

/* Convert a standard time_t to a SAS time */
double write_SAS_time(time_t);

#ifdef __cplusplus
}
#endif
#endif /* SAS7BDAT_H */
