/*
	libsas7bdat: minimalist library to read sas7bdat files
	Copyright (C) 2012 Bertrand Marc

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef SAS7BDAT_PAGE_H
#define SAS7BDAT_PAGE_H

/* Page types */
/* TODO champs de bits */
#define META 0
#define DATA 256
#define MIX 512
#define AMD 1024

struct subheader_pointer_t {
	int offset, length;
	int unknown; /* TODO last : last subheader */
};

struct any_page_t {
	int unknown1[4]; /* The first int is a page counter with a XOR mask = dataset->header.unknown6_3 */
	short type; /* TODO champs de bits */
	short nb_data_block;
};

struct meta_page_t {
	int unknown1[4];
	short type; /* TODO champs de bits ? */
	short nb_data_block;
	short nb_subheader_pointers;
	short zero; /* padding */
	struct subheader_pointer_t subheader_pointers[];
};

struct data_page_t {
	int unknown1[4];
	short type; /* TODO champs de bits */
	short row_count;
	int unknown2;
	char binary[];
};

union page_t {
	char raw[1];
	struct any_page_t any;
	struct meta_page_t meta;
	struct meta_page_t mix;
	struct meta_page_t amd;
	struct data_page_t data;
};

struct sas7bdat_handler_t;

int extract_meta(struct sas7bdat_handler_t*);
int read_next_page(struct sas7bdat_handler_t*);

#endif /* SAS7BDAT_PAGE_H */

