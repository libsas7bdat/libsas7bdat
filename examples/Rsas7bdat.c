/*
	Rsas7bdat: R package to convert a sas7bdat file to a R dataframe
	Copyright (C) 2012 Bertrand Marc

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <R.h>
#include <Rinternals.h>

#include "libsas7bdat/sas7bdat.h"

static int ncol, nrow, row;

/* Callback for numeric */
static int callback_num(void *df, int column, double num) {
	REAL(VECTOR_ELT(df, column - 1))[row] = num;
	return EXIT_SUCCESS;
}

/* Callback for characters */
static int callback_char(void *df, int column, int length, const char *text) {
	SET_STRING_ELT(VECTOR_ELT(df, column - 1), row, mkCharLen(text, length));
	return EXIT_SUCCESS;
}

/* Callback for the variables names */
static int callback_var(void *df, int column, int length, const char *text) {
	SET_STRING_ELT( getAttrib(df, R_NamesSymbol), column - 1, mkCharLen(text, length));
	return EXIT_SUCCESS;
}

/* Special callbacks to create the data frame on the first line */
static int callback_num_first(void *df, int column, double num) {
	SET_VECTOR_ELT(df, column - 1, allocVector(REALSXP, nrow));
	return callback_num(df, column, num);
}

static int callback_char_first(void *df, int column, int length, const char *text) {
	SET_VECTOR_ELT(df, column - 1, allocVector(STRSXP, nrow));
	return callback_char(df, column, length, text);
}

SEXP sas7bdat2df(SEXP filename) {
	SAS7BDAT *dataset = sas7bdat_init();
	if (dataset == NULL)
		Rf_error("could not allocate memory.");

	int res = sas7bdat_open(dataset, CHAR(STRING_ELT(filename, 0)));
	if(res != EXIT_SUCCESS)
		Rf_error("could not open file.");

	/* Write the variables names */
	ncol = sas7bdat_variables_number(dataset);
	nrow = sas7bdat_row_count(dataset);
	
	SEXP df, rownames, colnames, className;
	/* Create the data frame */
	PROTECT(df = allocVector(VECSXP, ncol));
	
	/* Create the rownames */
	PROTECT(rownames = allocVector(INTSXP, nrow));
	int i;
	for(i = 0; i < nrow; ++i)
		INTEGER(rownames)[i] = i + 1;
	setAttrib(df, R_RowNamesSymbol, rownames);
	
	/* Create the colnames */
	PROTECT(colnames = allocVector(STRSXP, ncol));
	setAttrib(df, R_NamesSymbol, colnames);

	/* Set the class attribute */
	PROTECT(className = allocVector(STRSXP, 1));
	SET_STRING_ELT(className, 0, mkChar("data.frame"));
	classgets(df, className);
	UNPROTECT(3);
	
	res = sas7bdat_variables_names(dataset, callback_var, df);
	if(res != EXIT_SUCCESS)
		Rf_error("could write the variables names.");

	/* Write data */
	row = 0;
	res = sas7bdat_next_row(dataset, callback_num_first, callback_char_first, df);
	for(row = 1; row < nrow; ++row) {
		res = sas7bdat_next_row(dataset, callback_num, callback_char, df);
		if (res != EXIT_SUCCESS)
			Rf_error("could not write data.");
	}
	
	/* Always cleanup */
	res = sas7bdat_close(dataset);
	UNPROTECT(1);
	return df;
}
