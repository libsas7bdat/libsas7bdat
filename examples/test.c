/*
	libsas7bdat: minimalist library to read sas7bdat files
	Copyright (C) 2012 Bertrand Marc

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#define VERBOSE
// #define NDEBUG
#include <assert.h>

#include "../data.h"
#include "../header.h"
#include "../subheader.h"
#include "../page.h"
#include "../foreign_system.h"
#include "../sas7bdat_struct.h"
#include "../sas7bdat.h"

int read_subheader(struct sas7bdat_handler_t *dataset, struct subheader_pointer_t *subheader_pointer) {
	if (subheader_pointer->length == 0)
		return EXIT_SUCCESS; /* ignore empty subheader */
	if ( subheader_pointer->offset > dataset->header.page_size ) {
		fprintf(stderr, "Error: subheader outside of range\n");
		fclose(dataset->fp);
		exit(EXIT_FAILURE);
	}
	union page_t *page = (union page_t*) (dataset->meta_pages->raw + (dataset->n_meta_pages - 1) * dataset->header.page_size);
	union subheader_t *subheader = (union subheader_t*) (page->raw + subheader_pointer->offset);
	int i, CMAX;
	if (dataset->header.endianness == 0)
		bswap_subheader(subheader);
	switch (subheader->type) {
		case(SUBH_ROWSIZE):
			assert( subheader->row_size.row_length < dataset->header.page_size );
			assert( subheader->row_size.test[2] == 0);
			//assert( subheader->row_size.test[3] == 0x23011 || subheader->row_size.test[3] == 0x223011);
#ifdef VERBOSE
			printf("ROWSIZE test %ld %ld %ld %lx\n", subheader->row_size.test[0], subheader->row_size.test[1], subheader->row_size.test[2], subheader->row_size.test[3] );
			printf("row length: %ld, row count: %ld, CC1: %ld, CC2: %ld\n",
				subheader->row_size.row_length, subheader->row_size.row_count,
				subheader->row_size.column_count1, subheader->row_size.column_count2);
			printf("max row count: %ld\n", subheader->row_size.max_row_count);
#endif
			assert( subheader->row_size.page_size == dataset->header.page_size );
			printf("%d, %d\n", subheader->row_size.end_of_header[0], subheader->row_size.end_of_header[1]);
			assert( subheader->row_size.end_of_header[0] == (SAS7BDAT_LONG) -1 && subheader->row_size.end_of_header[1] == (SAS7BDAT_LONG) -1 );
			printf("ROWSIZE mandatory short : %d\n", subheader->row_size.unknown5);
			break;
		case(SUBH_COLSIZE):
#ifdef VERBOSE
			printf("COLSIZE\n");
			printf("column count: %d\n", subheader->column_size.column_count);
#endif
			assert( subheader->column_size.zero == 0 );
			break;
		case(SUBH_COLTEXT):
			++dataset->column_text_subheaders.length;
			if (dataset->column_text_subheaders.length > 1) {
				void *ptr = realloc(dataset->column_text_subheaders.offset, sizeof(int) * dataset->column_text_subheaders.length);
				if (ptr == NULL) {
					fprintf(stderr, "Error: could not realloc memory.\n");
					return EXIT_FAILURE;
				}
				dataset->column_text_subheaders.offset = ptr;
			}
			dataset->column_text_subheaders.offset[dataset->column_text_subheaders.length - 1] = (char*) subheader - (char*) dataset->meta_pages;
#ifdef VERBOSE
			printf("COLTEXT\n");
			printf("remaining length %d\ntest: %x %x\nproc: %.60s, ascii: %.*s\n",
				subheader->column_text.remaining_length, subheader->column_text.unknown[0],
				subheader->column_text.unknown[1], subheader->column_text.proc_name,
				subheader_pointer->length - 76, subheader->column_text.names);
#endif
			break;
		case(SUBH_COLATTR):
			CMAX = (subheader->column_attributes.remaining_length - 8)/sizeof (struct column_attributes_t);
			assert( subheader->column_attributes.zero[0] == 0 );
			assert( subheader->column_attributes.zero[1] == 0 );
			assert( subheader->column_attributes.zero[2] == 0 );
#ifdef VERBOSE
			printf("COLATTR\n");
			printf("remaining length: %d, CMAX: %d\n",
				subheader->column_attributes.remaining_length, CMAX);
#endif
			for (i = 0; i < CMAX; ++i) {
				struct column_attributes_t* cap = subheader->column_attributes.column_attributes + i;
				assert( cap->zero == 0 );
#ifdef VERBOSE
				printf("\toffset: %d, width: %d, flag: %d, type: %x\n",
					cap->offset, cap->width, cap->name_length_flag, cap->type);
#endif
			}
			break;
		case(SUBH_COLNAME):
			++dataset->column_name_subheaders.length;
			if (dataset->column_name_subheaders.length > 1) {
				void *ptr = realloc(dataset->column_name_subheaders.offset, sizeof(int) * dataset->column_name_subheaders.length);
				if (ptr == NULL) {
					fprintf(stderr, "Error: could not realloc memory.\n");
					return EXIT_FAILURE;
				}
				dataset->column_name_subheaders.offset = ptr;
			}
			dataset->column_name_subheaders.offset[dataset->column_name_subheaders.length - 1] = (char*) subheader - (char*) dataset->meta_pages;
			CMAX = (subheader->column_name.remaining_length - 8)/sizeof (struct column_name_pointer_t);
			assert( subheader->column_name.zero[0] == 0 );
			assert( subheader->column_name.zero[1] == 0 );
			assert( subheader->column_name.zero[2] == 0 );
#ifdef VERBOSE
			printf("COLNAME\n");
			printf("remaining length: %d, CMAX: %d\n", subheader->column_name.remaining_length, CMAX);
#endif
			for (i = 0; i < CMAX; ++i) {
				struct column_name_pointer_t* cnp = subheader->column_name.column_name_pointer + i;
#ifdef VERBOSE
				printf("\tindex: %d, offset: %d, length: %d, sortedby: %d\n", cnp->index, cnp->offset, cnp->length, cnp->sortedby);
#endif
			}
			break;
		case(SUBH_COLLABS):
#ifdef VERBOSE
			printf("COLLABS\n");
			printf("\tformat index: %d, offset: %d, length: %d\n",
				subheader->column_format_label.format_index,
				subheader->column_format_label.format_offset,
				subheader->column_format_label.format_length);
			printf("\tlabel index: %d, offset: %d, length: %d\n",
				subheader->column_format_label.label_index,
				subheader->column_format_label.label_offset,
				subheader->column_format_label.label_length);
#endif
			break;
		case(SUBH_COLLIST):
#ifdef VERBOSE
			printf("COLLIST\n");
			printf("remaining_length: %d, %d, CC: %d, CL: %d, ?: %d, CC?: %d\n",
				subheader->column_list.remaining_length1,
				subheader->column_list.remaining_length2,
				subheader->column_list.column_count,
				subheader->column_list.column_list_length,
				subheader->column_list.unknown3,
				subheader->column_list.column_count2);
			for (i = 0; i < subheader->column_list.column_list_length; ++i) {
				printf("%d ", subheader->column_list.column_list_values[i]);
			}
			printf("\n");
#endif
			break;
		case(SUBH_SUBHCNT):
			assert(subheader_pointer->length == 304);
#ifdef VERBOSE
			printf("SUBHCNT\n");
			printf("offset: %ld, (4):%ld, (7):%d, zeros: %d, %d, %d\n",
				subheader->subheader_count.offset, subheader->subheader_count.unknown,
				subheader->subheader_count.unknown2, subheader->subheader_count.zeros[0],
				subheader->subheader_count.zeros[1], subheader->subheader_count.zeros[2]);
#endif
			for (i = 0; i < 11; ++i) {
#ifdef VERBOSE
				struct subheader_count_vector_t *cv = subheader->subheader_count.subheader_count_vector + i;
				printf("\tsignature: %x page1: %d, loc1: %d, pagel: %d, locl: %d\n",
					cv->signature, cv->page1, cv->loc1, cv->pagel, cv->locl);
#endif
			}
			break;
		default:
			fprintf(stderr, "Warning: unknown subheader %x\n", subheader->type);
	}
	return EXIT_SUCCESS;
}

void read_pages(struct sas7bdat_handler_t *dataset) {
	/* Read pages TODO */
	dataset->column_text_subheaders.length = 0;
	dataset->column_name_subheaders.length = 0;
	dataset->column_text_subheaders.offset = (int*) malloc(sizeof(int));
	dataset->column_name_subheaders.offset = (int*) malloc(sizeof(int));
	
	dataset->reader.page = malloc (sizeof(char) * dataset->header.page_size);
	int i, page_nb;
	int nb_mix=0, nb_meta=0, nb_data=0, nb_amd=0;
	
	for (page_nb = 0; page_nb < dataset->header.page_count; ++page_nb) {
		fread(dataset->reader.page, dataset->header.page_size, 1, dataset->fp);
		if ( dataset->header.endianness == 0 )
			bswap_page(dataset->reader.page);
		assert((atof(dataset->header.release) < 8) || ((page_nb+1) ^ dataset->header.unknown6_3 ) == dataset->reader.page->any.unknown1[0] );
#ifdef VERBOSE
		printf("Page type: %d, n %d test %08x %08x\n", dataset->reader.page->any.type, page_nb, dataset->reader.page->any.unknown1[0] ^ dataset->header.unknown6_3, (page_nb+1) );
#endif
		if (dataset->reader.page->any.type == MIX) {
			++nb_mix;
			//printf("MIX\n");
		} else if (dataset->reader.page->any.type == META) {
			++nb_meta;
			//printf("META\n");
		} else if (dataset->reader.page->any.type == DATA) {
			++nb_data;
			//printf("DATA\n");
		} else if (dataset->reader.page->any.type == AMD) {
			++nb_amd;
			//printf("AMD\n");
		}
		assert(dataset->reader.page->any.type != META || nb_mix == 0); /* META pages are always before any MIX page */
		assert((dataset->reader.page->any.type != META && dataset->reader.page->any.type != MIX) || nb_data == 0 ); /* META and MIX pages are always before any DATA page */
		if (dataset->reader.page->any.type == MIX || dataset->reader.page->any.type == META ) {
			/* Copy page from cache to meta_pages */
			++dataset->n_meta_pages;
			void *ptr = realloc(dataset->meta_pages, dataset->header.page_size * dataset->n_meta_pages);
			if (ptr == NULL) {
				fprintf(stderr, "Error: could not realloc memory.\n");
				exit(EXIT_FAILURE);
			}
			dataset->meta_pages = ptr;
			union page_t *page = (union page_t*) (dataset->meta_pages->raw + (dataset->n_meta_pages - 1) * dataset->header.page_size);
			memcpy(page, dataset->reader.page, dataset->header.page_size);
			
			assert( dataset->reader.page->mix.zero == 0 );
			assert( dataset->reader.page->any.type != META || dataset->reader.page->mix.nb_data_block == dataset->reader.page->mix.nb_subheader_pointers);
#ifdef VERBOSE
			printf("test: %d\n", dataset->reader.page->mix.nb_data_block);
			printf("nb_subheader_pointers: %d\n", dataset->reader.page->mix.nb_subheader_pointers);
#endif
			for (i = 0; i < dataset->reader.page->mix.nb_subheader_pointers; i++) {
#ifdef VERBOSE
				printf("offset: %d, length: %d, unknown: %x\n", dataset->reader.page->mix.subheader_pointers[i].offset,
					dataset->reader.page->mix.subheader_pointers[i].length, dataset->reader.page->mix.subheader_pointers[i].unknown);
#endif
				read_subheader(dataset, &(dataset->reader.page->mix.subheader_pointers[i]));
			}
		}
	}
	/* assert(nb_mix==0 || nb_meta==0); FALSE !! */
	assert(nb_mix <= 1);
	/* assert(nb_meta<=1); FALSE */

	free(dataset->reader.page);

	/* Read column names */
#ifdef VERBOSE
	if (dataset->column_text_subheaders.length && dataset->column_name_subheaders.length) {
		printf("columns names\n");
		int i, j, CMAX;
		for (j = 0; j < dataset->column_name_subheaders.length; ++j) {
			struct column_name_subheader_t *cns = (struct column_name_subheader_t *) (dataset->meta_pages->raw + dataset->column_name_subheaders.offset[j]);
			printf("CNS offset %ld, %d\n", (long int)((void*)cns-(void*)dataset->meta_pages), dataset->column_name_subheaders.offset[j]);
			CMAX = (cns->remaining_length - 8)/sizeof (struct column_name_pointer_t);
			printf("ICI j=%d /%d, CMAX=%d\n", j+1, dataset->column_name_subheaders.length, CMAX);
			for (i = 0; i < CMAX; ++i) {
				struct column_name_pointer_t* cnp = cns->column_name_pointer + i;
				struct column_text_subheader_t *cts = (struct column_text_subheader_t *) (dataset->meta_pages->raw + dataset->column_text_subheaders.offset[cnp->index]);
				printf("%.*s ", cnp->length,  ((char*) cts) + cnp->offset + 4);
			}
		}
		printf("\n");
		/* TODO labels and formats */
	}
#endif
	/* Closing */
	free(dataset->column_text_subheaders.offset);
	free(dataset->column_name_subheaders.offset);
}

/* Read data : retain column attribute subheaders, and read pages */
int read_data(struct sas7bdat_handler_t *dataset) {
	int attributes_nb = 0;
	struct column_attributes_t *attributes = NULL;
	struct row_size_subheader_t row_size;
	int nb_row_size=0;
	dataset->reader.page = malloc (sizeof(char) * dataset->header.page_size);
	int i, page_nb;
	for (page_nb = 0; page_nb < dataset->header.page_count; ++page_nb) {
		fread(dataset->reader.page, dataset->header.page_size, 1, dataset->fp);
		if (dataset->reader.page->any.type == MIX || dataset->reader.page->any.type == META) {
			/* Copy page from cache to meta_pages */
			++dataset->n_meta_pages;
			void *ptr = realloc(dataset->meta_pages, dataset->header.page_size * dataset->n_meta_pages);
			if (ptr == NULL) {
				fprintf(stderr, "Error: could not realloc memory.\n");
				return EXIT_FAILURE;
			}
			dataset->meta_pages = ptr;
			union page_t *page = (union page_t*) (dataset->meta_pages->raw + (dataset->n_meta_pages - 1) * dataset->header.page_size);
			memcpy(page, dataset->reader.page, dataset->header.page_size);
			printf("PAGE copied.................\n");
			for (i = 0; i < page->mix.nb_subheader_pointers; ++i) {
				if (page->mix.subheader_pointers[i].length == 0)
					continue; /* ignore empty subheader */
				if ( page->mix.subheader_pointers[i].offset > dataset->header.page_size ) {
					fprintf(stderr, "Error: subheader outside of range\n");
					fclose(dataset->fp);
					exit(EXIT_FAILURE);
				}
				union subheader_t *subheader = (union subheader_t*) (((char*) page) + page->mix.subheader_pointers[i].offset);
				if (subheader->type == SUBH_COLATTR) {
					size_t length = subheader->column_attributes.remaining_length - 8;
					int old_attributes_nb = attributes_nb;
					if (attributes_nb == 0) {
						attributes_nb = length/sizeof (struct column_attributes_t);
						attributes = malloc(sizeof(struct column_attributes_t) * attributes_nb);
						memcpy(attributes, subheader->column_attributes.column_attributes, length);
					} else {
						attributes_nb += length/sizeof (struct column_attributes_t);
						attributes = realloc(attributes, sizeof(struct column_attributes_t) * attributes_nb);
						/* TODO test NULL, with tmp ptr */
						memcpy(attributes + old_attributes_nb, subheader->column_attributes.column_attributes, length);
					}
				}
				if (subheader->type == SUBH_ROWSIZE) {
					assert(nb_row_size == 0); /* Only one row size subheader */
					row_size = subheader->row_size;
				}
			}
		}
		if (dataset->reader.page->any.type == DATA || dataset->reader.page->any.type == MIX) {
			char *data;
			if (dataset->reader.page->any.type == MIX)
				data = ((char*) dataset->reader.page) + 24 + dataset->reader.page->mix.nb_subheader_pointers * 12 + (24 + dataset->reader.page->mix.nb_subheader_pointers * 12) % 8;
			else
				data = ((char*) dataset->reader.page) + 24;
			/* use attributes to read data */
			int row, row_max = row_size.max_row_count>row_size.row_count?row_size.row_count:row_size.max_row_count;
			for (row = 0; row < row_max; ++row) {
				printf("row %d :", row + 1);
				for (i = 0; i < attributes_nb; ++i) {
					if (attributes[i].type == 1) {
						double tmp = read_SAS_numeric(data, attributes[i].width);
						printf("%.2f, ", tmp);
					} else
						printf("%.*s,", attributes[i].width, (char*) data);
					data += attributes[i].width;
				}
				printf("\n");
			}
			row_size.row_count = row_size.row_count>row_max?row_size.row_count - row_max:0;
		}
	}
	// printf("NB attributes: %d\n", attributes_nb);
	if (attributes != NULL)
		free(attributes);
	free(dataset->reader.page);
	return EXIT_SUCCESS;
}

void read_sas7bdat(char *filename) {
	struct sas7bdat_handler_t *dataset = sas7bdat_init();
	dataset->filename = filename;
	dataset->fp = fopen(dataset->filename, "rb");
	dataset->meta_pages = NULL;
	if(dataset->fp == NULL) {
		fprintf(stderr, "Cannot open file %s : %s\n", dataset->filename, strerror(errno));
		exit(EXIT_FAILURE);
	}

	assert(sizeof(struct header64_t) == 1024);
	size_t read = fread(&(dataset->header), sizeof(struct header64_t), 1, dataset->fp);
	if (read != 1) {
		if ( feof(dataset->fp) )
			fprintf(stderr, "File too short %s.\n", dataset->filename);
		if ( ferror(dataset->fp) )
			fprintf(stderr, "Error reading file %s.\n", dataset->filename);
		fclose(dataset->fp);
		exit(EXIT_FAILURE);
	}
	
	read_header(dataset);
	/* Handle headers longer than 1024 */
	if (dataset->header.header_size != sizeof(struct header64_t))
		fseek(dataset->fp, dataset->header.header_size, SEEK_SET);
	dataset->n_meta_pages = 0;
	dataset->column_text_subheaders.length = 0;
	dataset->column_name_subheaders.length = 0;
	dataset->meta_pages = NULL;
	read_pages(dataset);
	
	// fseek(dataset->fp, dataset.header.header_size, SEEK_SET);
	// read_data(dataset);
	fclose(dataset->fp);
	free(dataset);
}

void check_sizes() {
	assert(sizeof(struct header64_t) == 1024);
	assert(sizeof(struct header_ILP32) == 1024);
	assert(sizeof(struct row_size_subheader_t) == 480);
	assert(sizeof(struct column_size_subheader_t) == 12);
	assert(sizeof(struct subheader_count_subheader_t) == 304);
	assert(sizeof(struct subheader_count_vector_t) == 20);
}

int main(int argc, char * argv[]) {
	check_sizes();
	if ( argc == 1 ) {
#ifdef VERBOSE
		printf("Opening default file : applican.sas7bdat\n");
#endif
		read_sas7bdat("../datasets/applican.sas7bdat");
	} else {
		read_sas7bdat(argv[1]);
		// printf("%s\n", argv[1]);
	}	
	return EXIT_SUCCESS;
}
