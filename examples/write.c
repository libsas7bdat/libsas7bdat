#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <errno.h>
#include <unistd.h>
#include <assert.h>

#include "../header.h"
#include "../page.h"
#include "../sas7bdat_struct.h"
#include "../sas7bdat.h"

#define PAGE_SIZE 4096

void check_sizes() {
	assert(sizeof(struct header64_t) == 1024);
	assert(sizeof(struct row_size_subheader_t) == 480);
	assert(sizeof(struct column_size_subheader_t) == 12);
	assert(sizeof(struct subheader_count_subheader_t) == 304);
	assert(offsetof(struct header_ILP32, timestamp) == 164);
	assert(offsetof(struct header_ILP32, release) == 216);
}

int main() {
	check_sizes();
	/* input:
		- filename
		- vector length
		
	*/
	size_t page_size = PAGE_SIZE;/* sysconf(_SC_PAGESIZE); */
	char filename[] = "machin.sas7bdat";
	struct sas7bdat_handler_t *dataset = sas7bdat_init();
	dataset->filename = filename;
	dataset->reader.page = malloc(sizeof(char) * page_size);
	if(dataset->reader.page == NULL) {
		fprintf(stderr, "Error: could not alloc memory.\n");
		return EXIT_FAILURE;
	}
	printf("TEST2\n");
	_sas7bdat_init_header(dataset);
	dataset->fp = fopen(filename, "wb");
	if(dataset->fp == NULL) {
		fprintf(stderr, "Error: could not open file %s, %s.\n", filename, strerror(errno));
		return EXIT_FAILURE;
	}
	printf("TEST3\n");
	fwrite(&dataset->header, sizeof(struct header_ILP32), 1, dataset->fp);

	/* TODO Compute row length */
	int row_length = 0;
	int row_count = 1;
	int column_count = 0;

	/* Write pages */
	dataset->reader.page_n = 1;
	dataset->reader.page->any.type = MIX;
	dataset->reader.page->meta.unknown1[0] = dataset->reader.page_n ^ dataset->header.unknown6_3;
	dataset->reader.page->meta.nb_subheader_pointers = 0;

	void *subheader = (void*) (dataset->reader.page) + page_size;

	/* ROW_SIZE */
	subheader -= sizeof(struct row_size_subheader_t);
	struct row_size_subheader_t *row_size = (struct row_size_subheader_t*) (subheader);
	row_size->type = SUBH_ROWSIZE;
	row_size->test[0] = 240;
	//row_size->test[1] = 7;
	row_size->row_length = row_length;
	row_size->row_count = row_count;
	row_size->column_count1 = column_count;
	row_size->column_count2 = 0;
	row_size->page_size = page_size;
	row_size->max_row_count = 2808;
	row_size->end_of_header[0] = -1;
	row_size->end_of_header[1] = -1;
	row_size->page_signature[0] = dataset->reader.page->meta.unknown1[0];
	row_size->unknown4 = 1;
	row_size->unknown5 = 2;
	row_size->pages_with_subheaders = 1;
	row_size->subheaders_with_positive_length_on_last_page = 4;
	//row_size->pages_with_subheaders2 = row_size->pages_with_subheaders;
	//row_size->subheaders_with_positive_length_on_last_page2 = 6;
	row_size->page_count = 1;
	row_size->unknown6 = 6;
	row_size->coltext_number = 1;
	/* subheader pointer */
	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].offset = subheader - (void*) dataset->reader.page;
	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].length = sizeof(struct row_size_subheader_t);
	++dataset->reader.page->meta.nb_subheader_pointers;

	/* COL_SIZE */
	subheader -= sizeof(struct column_size_subheader_t);
	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].length = sizeof(struct column_size_subheader_t);
	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].offset = subheader - (void*) dataset->reader.page;
	struct column_size_subheader_t *col_size = (struct column_size_subheader_t*) (subheader);
	col_size->type = SUBH_COLSIZE;
	col_size->column_count = column_count;
	++dataset->reader.page->meta.nb_subheader_pointers;

	/* SUBH_CNT */
	subheader -= sizeof(struct subheader_count_subheader_t);
	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].length = sizeof(struct subheader_count_subheader_t);
	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].offset = subheader - (void*) dataset->reader.page;
	struct subheader_count_subheader_t *sub_cnt = (struct subheader_count_subheader_t*) (subheader);
	sub_cnt->type = SUBH_SUBHCNT;
	sub_cnt->offset = 36;
	sub_cnt->unknown = 1L;
	sub_cnt->unknown2 = 7;
	sub_cnt->subheader_count_vector[0] = (struct subheader_count_vector_t) { .signature = -4, .page1 = 0, .loc1 = 0, .pagel = 0, .locl = 0 };
	sub_cnt->subheader_count_vector[1] = (struct subheader_count_vector_t) { .signature = -3, .page1 = 1, .loc1 = 4, .pagel = 1, .locl = 4 };
	sub_cnt->subheader_count_vector[2] = (struct subheader_count_vector_t) { .signature = -1, .page1 = 0, .loc1 = 0, .pagel = 0, .locl = 0 };
	sub_cnt->subheader_count_vector[3] = (struct subheader_count_vector_t) { .signature = -2, .page1 = 0, .loc1 = 0, .pagel = 0, .locl = 0 };
	sub_cnt->subheader_count_vector[4] = (struct subheader_count_vector_t) { .signature = -5, .page1 = 0, .loc1 = 0, .pagel = 0, .locl = 0 };
	sub_cnt->subheader_count_vector[5] = (struct subheader_count_vector_t) { .signature = -6, .page1 = 0, .loc1 = 0, .pagel = 0, .locl = 0 };
	sub_cnt->subheader_count_vector[6] = (struct subheader_count_vector_t) { .signature = -7, .page1 = 0, .loc1 = 0, .pagel = 0, .locl = 0 };
	++dataset->reader.page->meta.nb_subheader_pointers;

	/* COLTEXT */
	subheader -= 48;
	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].length = 48;
	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].offset = subheader - (void*) dataset->reader.page;
	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].unknown = 256;
	struct column_text_subheader_t *col_text = (struct column_text_subheader_t*) (subheader);
	col_text->type = SUBH_COLTEXT;
	col_text->remaining_length = 48 - 12;
	/* Text should be 4 bytes aligned */
	strncpy(subheader + 12, "                DATASTEP", 24);
	++dataset->reader.page->meta.nb_subheader_pointers;

	/* COLNAME */
	/*subheader -= 28;
	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].length = 28;
	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].offset = subheader - (void*) dataset->reader.page;
	struct column_name_subheader_t *col_name = (struct column_name_subheader_t*) (subheader);
	col_name->type = SUBH_COLNAME;
	col_name->remaining_length = 28 - 12;
	col_name->column_name_pointer[0] = (struct column_name_pointer_t) { .index = 0, .offset = 32, .length = 4, .sortedby = 0 };
	++dataset->reader.page->meta.nb_subheader_pointers;*/

	/* COLATTR */
	/*subheader -= 28;
	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].length = 28;
	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].offset = subheader - (void*) dataset->reader.page;
	struct column_name_subheader_t *col_name = (struct column_name_subheader_t*) (subheader);
	col_name->type = SUBH_COLNAME;
	col_name->remaining_length = 28 - 12;
	col_name->column_name_pointer[0] = (struct column_name_pointer_t) { .index = 0, .offset = 32, .length = 4, .sortedby = 0 };
	++dataset->reader.page->meta.nb_subheader_pointers;*/

	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].length = 0;
	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].offset = subheader - (void*) dataset->reader.page;
	dataset->reader.page->meta.subheader_pointers[dataset->reader.page->meta.nb_subheader_pointers].unknown = 1;
	++dataset->reader.page->meta.nb_subheader_pointers;

	dataset->reader.page->any.nb_data_block = dataset->reader.page->meta.nb_subheader_pointers;

	/* TEST */
	/* FILE *fp = fopen("../datasets/test32.sas7bdat", "rb");
	fseek(fp, 1024, SEEK_SET);
	fread(dataset->reader.page, 20, 1, fp);
	fclose(fp);
	dataset->reader.page->meta.unknown1[0] = dataset->reader.page_n ^ dataset->header.unknown6_3; */
	/* /TEST */

	fwrite(dataset->reader.page, page_size, 1, dataset->fp);
	++dataset->reader.page_n;
	/* finalize */
	free(dataset->reader.page);
	//fseek(dataset->fp, 0, SEEK_SET);
	//fwrite(&dataset->header, sizeof(struct header_ILP32), 1, dataset->fp);
	fclose(dataset->fp);
	return EXIT_SUCCESS;
}

