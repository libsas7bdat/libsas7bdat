/*
	sas7bdat2csv: minimalist example of libsas7bdat use
	Copyright (C) 2012 Bertrand Marc

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "../sas7bdat.h"

/* Demonstration use of the libsas7bdat API to convert a sas7bdat to a csv */
static int n_variables;

/* Callback for numeric */
int print_num(void *fp, int column, double num) {
	if (column < n_variables)
		return fprintf(fp, "%f,", num);
	else
		return fprintf(fp, "%f\n", num);
}

/* Callback for characters */
int print_char(void *fp, int column, int length, const char *text) {
	if (column < n_variables)
		return fprintf(fp, "%.*s,", length, text);
	else
		return fprintf(fp, "%.*s\n", length, text);
}

int main(int argc, char * argv[]) {
	if ( argc != 3 ) {
		fprintf(stderr, "Usage: sas7bdat2csv INFILE.sas7bdat OUTFILE.csv\n");
		return EXIT_FAILURE;
	}
	SAS7BDAT *dataset = sas7bdat_init();
	if (dataset == NULL)
		return EXIT_FAILURE;

	int res = sas7bdat_open(dataset, argv[1]);
	if(res != EXIT_SUCCESS)
		return res;

	FILE *fp = fopen(argv[2], "w");
	if(fp == NULL) {
		fprintf(stderr, "Cannot open file %s : %s\n", argv[1], strerror(errno));
		return EXIT_FAILURE;
	}

	/* Write the variables names */
	n_variables = sas7bdat_variables_number(dataset);
	res = sas7bdat_variables_names(dataset, print_char, fp);
	if(res != EXIT_SUCCESS)
		return res;

	/* Write data */
	int i, nrows = sas7bdat_row_count(dataset);
	for(i = 0; i < nrows; ++i) {
		res = sas7bdat_next_row(dataset, print_num, print_char, fp);
		if (res != EXIT_SUCCESS)
			return res;
	}

	/* Always cleanup */
	res = sas7bdat_close(dataset);
	fclose(fp);
	return res;
}
