/*
	libsas7bdat: minimalist library to read sas7bdat files
	Copyright (C) 2012 Bertrand Marc

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef SAS7BDAT_FOREIGN_H
#define SAS7BDAT_FOREIGN_H

#if defined(_WIN32) || defined(_WIN64)
	unsigned short bswap_16 (unsigned short );
	unsigned int bswap_32 (unsigned int);
	unsigned long long bswap_64 (unsigned long long);
#else
	#include <byteswap.h>
#endif

#include "header.h"
void bswap_header(struct header64_t*);
void convert_32(struct header64_t*);
void convert_SunOS(struct header64_t*);

#include "page.h"
void bswap_page(union page_t*);

#include "subheader.h"
void bswap_subheader(union subheader_t*);

#endif /* SAS7BDAT_FOREIGN_H */
